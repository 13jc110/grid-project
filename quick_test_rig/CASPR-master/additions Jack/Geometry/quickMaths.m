function quickMaths
c12 = 87.5;
c23 = 82.5;
c34 = 85.5;
c41 = 85;
c1 = 59.06;
c2 = 61.44;
c3 = 61.6;
c4 = 58.74;
c = [c12' c23' c34' c41' c12'];
l = [c1' c2' c3' c4];
c13 = 120.7;
c24 = 120.2;
d = [c13' c24' c13' c24'];
%This determines the 360 degree of the uneven squre to see accuracy of measurements
angle = 0;
for i = 1:4
    angle = angle + acosd(-1.*(d(:,i).^2-c(:,i).^2-c(:,i+1).^2)./2./c(:,i)./c(:,i+1));
end

%Assuming x axis is parallel to tower 2 and 3
a23 = acosd(-1.*(c3.^2-c2.^2-c23.^2)./2./c2./c23);
a32 = acosd(-1.*(c2.^2-c3.^2-c23.^2)./2./c3./c23);
a12 = acosd(-1.*(c2.^2-c1.^2-c12.^2)./2./c1./c12);
a21 = acosd(-1.*(c1.^2-c2.^2-c12.^2)./2./c2./c12);
a34 = acosd(-1.*(c4.^2-c3.^2-c34.^2)./2./c3./c34);
a43 = acosd(-1.*(c3.^2-c4.^2-c34.^2)./2./c4./c34);
a14 = acosd(-1.*(c4.^2-c1.^2-c41.^2)./2./c1./c41);
a41 = acosd(-1.*(c1.^2-c4.^2-c41.^2)./2./c4./c41);

x1error = cosd(a23+a21)
y1error = sind(a23+a21)

end
