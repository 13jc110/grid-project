function gcode_to_motor_turns
close all
clc
[~, id] = lastwarn;
warning('off', id)
%% Initialized Variables
%Still need to make a program which converst G-Code to readable data for MATLAB
%For now we will call sample trajectories for trajectoriesConverter
collectdata = 'traj_indexsquare'; %Call which trajectory to find cable lengths for:
%textName = 'Expendable Text.txt';
trials = [traj_fullsquare; traj_indexsquare]; %Creates XML with given trajectories (;;;;;;;;)

winchRadius = 0.01; %average winch radius
cableOrigin = [0.741103569 0.7340115735 0.7366376178 0.74156614]; %Needs to be identical to CASPR (format long)
pronterOrigin = cableOrigin*(40/2/pi/winchRadius);
motorStep = 1; %1 for full step, 2 for half step
%Full Step in 1.8 degree per step and half step is 0.9 degree per step
%fileLocation = 'C:\Users\Jack\Desktop\';
%textFile = [fileLocation,textName];
%% XML Convertor: Temporary Replacement until program can read example G-Code, can make full matrix if need be

%Do not edit code below
n = com.mathworks.xml.XMLUtils.createDocument('trajectories');
domImpl = n.getImplementation();
doctype = domImpl.createDocumentType('trajectories','','../../../../templates/trajectories.dtd');
n.appendChild(doctype);

x1 = n.getDocumentElement;
    x2 = n.createElement('joint_trajectories');
    for j = 1:1:size(trials,1)
        x = cell2mat(trials(j,1));
        y = cell2mat(trials(j,2));
        z = cell2mat(trials(j,3));
        t = cell2mat(trials(j,4));
        name = cell2mat(trials(j,5));
        tstep = cell2mat(trials(j,6));
        num = length(x);
        xyz_array = [x',y',z',zeros(num,2)];
        x3 = n.createElement('quintic_spline_trajectory');
        x3.setAttribute('time_definition','relative');
        x3.setAttribute('id',name);
        x3.setAttribute('time_step',num2str(tstep));
            x4 = n.createElement('points');
                x5 = n.createElement('point');
                    x6 = n.createElement('q');
                    x6.appendChild(n.createTextNode(sprintf('%.2f ',xyz_array(1,:))));
                    x6.appendChild(n.createTextNode(sprintf('%.1f',0)));
                    x5.appendChild(x6);
                    x6 = n.createElement('q_dot');
                    x6.appendChild(n.createTextNode(sprintf('%.1f ',[0 0 0 0 0])));
                    x6.appendChild(n.createTextNode(sprintf('%.1f',0)));
                    x5.appendChild(x6);
                    x6 = n.createElement('q_ddot');
                    x6.appendChild(n.createTextNode(sprintf('%.1f ',[0 0 0 0 0])));
                    x6.appendChild(n.createTextNode(sprintf('%.1f',0)));
                    x5.appendChild(x6);
                x4.appendChild(x5);
                for i = 2:1:num
                x5 = n.createElement('point');
                x5.setAttribute('time',num2str(t(i)))
                    x6 = n.createElement('q');
                    x6.appendChild(n.createTextNode(sprintf('%.2f ',xyz_array(i,:))));
                    x6.appendChild(n.createTextNode(sprintf('%.1f',0)));
                    x5.appendChild(x6);
                    x6 = n.createElement('q_dot');
                    x6.appendChild(n.createTextNode(sprintf('%.1f ',[0 0 0 0 0])));
                    x6.appendChild(n.createTextNode(sprintf('%.1f',0)));
                    x5.appendChild(x6);
                    x6 = n.createElement('q_ddot');
                    x6.appendChild(n.createTextNode(sprintf('%.1f ',[0 0 0 0 0])));
                    x6.appendChild(n.createTextNode(sprintf('%.1f',0)));
                    x5.appendChild(x6);
                x4.appendChild(x5);
                end 
            x3.appendChild(x4);
        x2.appendChild(x3);
    end
    x1.appendChild(x2);
    
xmlFileName = ...
    ['C:\Users\Jack\Desktop\CASPR-master\data\model_config\models\SCDM\spatial_manipulators\Joom_3D\Joom_3D_trajectories','.xml'];
xmlwrite(xmlFileName,n);
%% Script to call CASPR to calculate cable lengths and cable lengths dot

% Set up the type of model
model_config = ModelConfig('Joom 3D');
cable_set_id = 'original';
trajectory_id = collectdata;

% Load the SystemKinematics object from the XML
modelObj = model_config.getModel(cable_set_id);

% Setup the inverse kinematics simulator with the SystemKinematics object
disp('Start Setup Simulation');
sim = InverseKinematicsSimulator(modelObj);
trajectory = model_config.getJointTrajectory(trajectory_id);
disp('Finished Setup Simulation');

% Run the kinematics on the desired trajectory
disp('Start Running Simulation');
sim.run(trajectory);
disp('Finished Running Simulation');

% After running the simulator the data can be plotted
% Refer to the simulator classes to see what can be plotted.
disp('Start Plotting Simulation');
cableLengths = sim.plotCableLengths();

%% Converting Cable Lengths to Pronter Face Values

b = pronterOrigin;
fprintf('\nPronter Origin is: X%.1f Y%.1f Z%.1f E%.1f\n',b(3),b(1),b(4),b(3))
fprintf('\n')

%%In case we need the addition of a pause in the system. Doesnt Work Properly Still so be careful
% for j = 2:length(t)
%     for i = 1:t(j)/tstep
%         k = (j-2)*t(j)/tstep;
%         pronterFace(i+k,:) = cableLengths(i+k,:)*motorStep*40/2/pi/winchRadius;
%         a = pronterFace;
%         fprintf('G00 X%.1f Y%.1f Z%.1f E%.1f\n',a(i+k,3),a(i+k,1),a(i+k,4),a(i+k,2))
%     end
%     if j ~= length(t) 
%     fprintf('G4 S1\n')
%     end
% end

for i=1:size(cableLengths,1)
    pronterFace(i,:) = cableLengths(i,:)*motorStep*40/2/pi/winchRadius;
    a = pronterFace;
    fprintf('G00 X%.1f Y%.1f Z%.1f E%.1f\n',a(i,3),a(i,1),a(i,4),a(i,2))
end


end