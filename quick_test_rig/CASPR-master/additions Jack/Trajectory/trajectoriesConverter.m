
%Only Edit you need to make is trials list.
%You can choose how many trajectory samples that will be listed on the GUI, do add more functions
%Make sure separated by semi-colon, NOT coma or space

trials = [traj_1; traj_z_up; traj_y_up_slow];


%Do not edit code below
n = com.mathworks.xml.XMLUtils.createDocument('trajectories');
domImpl = n.getImplementation();
doctype = domImpl.createDocumentType('trajectories','','../../../../templates/trajectories.dtd');
n.appendChild(doctype);

x1 = n.getDocumentElement;
    x2 = n.createElement('joint_trajectories');
    for j = 1:1:size(trials,1)
        x = cell2mat(trials(j,1));
        y = cell2mat(trials(j,2));
        z = cell2mat(trials(j,3));
        t = cell2mat(trials(j,4));
        name = cell2mat(trials(j,5));
        tstep = cell2mat(trials(j,6));
        num = length(x);
        xyz_array = [x',y',z',zeros(num,2)];
        x3 = n.createElement('quintic_spline_trajectory');
        x3.setAttribute('time_definition','relative');
        x3.setAttribute('id',name);
        x3.setAttribute('time_step',num2str(tstep));
            x4 = n.createElement('points');
                x5 = n.createElement('point');
                    x6 = n.createElement('q');
                    x6.appendChild(n.createTextNode(sprintf('%.2f ',xyz_array(1,:))));
                    x6.appendChild(n.createTextNode(sprintf('%.1f',0)));
                    x5.appendChild(x6);
                    x6 = n.createElement('q_dot');
                    x6.appendChild(n.createTextNode(sprintf('%.1f ',[0 0 0 0 0])));
                    x6.appendChild(n.createTextNode(sprintf('%.1f',0)));
                    x5.appendChild(x6);
                    x6 = n.createElement('q_ddot');
                    x6.appendChild(n.createTextNode(sprintf('%.1f ',[0 0 0 0 0])));
                    x6.appendChild(n.createTextNode(sprintf('%.1f',0)));
                    x5.appendChild(x6);
                x4.appendChild(x5);
                for i = 2:1:num
                x5 = n.createElement('point');
                x5.setAttribute('time',num2str(t(i)))
                    x6 = n.createElement('q');
                    x6.appendChild(n.createTextNode(sprintf('%.2f ',xyz_array(i,:))));
                    x6.appendChild(n.createTextNode(sprintf('%.1f',0)));
                    x5.appendChild(x6);
                    x6 = n.createElement('q_dot');
                    x6.appendChild(n.createTextNode(sprintf('%.1f ',[0 0 0 0 0])));
                    x6.appendChild(n.createTextNode(sprintf('%.1f',0)));
                    x5.appendChild(x6);
                    x6 = n.createElement('q_ddot');
                    x6.appendChild(n.createTextNode(sprintf('%.1f ',[0 0 0 0 0])));
                    x6.appendChild(n.createTextNode(sprintf('%.1f',0)));
                    x5.appendChild(x6);
                x4.appendChild(x5);
                end 
            x3.appendChild(x4);
        x2.appendChild(x3);
    end
    x1.appendChild(x2);
    
xmlFileName = ['C:\Users\Jack\Desktop\CASPR-master\data\model_config\models\SCDM\spatial_manipulators\Joom_3D\Joom_3D_trajectories','.xml'];
xmlwrite(xmlFileName,n);


