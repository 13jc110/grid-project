function c = traj_fullsquare

name = 'traj_fullsquare';
tstep = 5;
l = 0.3;
h = 0.01;
s = 10;

t = [ 0  s  s   3*s   3*s   3*s  s  s ];
x = [ 0  0  -l  -l  l   l  0  0 ];
y = [ 0  l  l   -l  -l  l  l  0 ];
z = [ h  h  h   h   h   h  h  h ];

c = {x y z t name tstep};

end