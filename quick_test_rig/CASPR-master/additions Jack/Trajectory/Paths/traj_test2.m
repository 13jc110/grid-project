function c = traj_test2

%Make sure this name is identical to the function name
name = 'traj_test2';
tstep = 0.5;

%Sample code to make for this
xval = [-0.1 0.1];
yval = [-0.1:0.01:0.1];
zval = [0.02 0.04 0.06 0.08 0.10];
% xval = [0 1 2];
% yval = [0 2 4];
% zval = [0 5 10];
timex = 5;
timey = 0.5;

%For now keep ny = nx
nx = length(xval);
ny = length(yval);
nz = length(zval);
t = zeros(1,(length(xval)*length(yval)*length(zval)));
x = t; y = t; z = t;

% for l = 2:(length(xval)*length(yval)*length(zval))
%     t(l) = time;
% end

for m = 1:nz
    if mod(m,2) ~= 0
        for i = 1:nx
            x(i+nx*ny*(m-1)) = xval(i);
            y(i+nx*ny*(m-1)) = yval(1);
            z(i+nx*ny*(m-1)) = zval(m);
            t(i+nx*ny*(m-1)) = timex;
        end 
        for j = 1:ny-1
            x(j*nx+1+nx*ny*(m-1)) = x(j*nx+nx*ny*(m-1));
            y(j*nx+1+nx*ny*(m-1)) = yval(j+1);
            z(j*nx+1+nx*ny*(m-1)) = zval(m);
            t(j*nx+1+nx*ny*(m-1)) = timey;
            for k = 2:nx
                if mod(j,2) ~= 0
                    x(j*nx+k+nx*ny*(m-1)) = xval(nx+1-k);
                else
                    x(j*nx+k+nx*ny*(m-1)) = xval(k);
                end
                y(j*nx+k+nx*ny*(m-1)) = yval(j+1);
                z(j*nx+k+nx*ny*(m-1)) = zval(m);
                t(j*nx+k+nx*ny*(m-1)) = timex;
            end
        end
    else
        if mod(ny,2)== 0
            for i = 1:nx
                x(i+nx*ny*(m-1)) = xval(i);
                y(i+nx*ny*(m-1)) = yval(ny);
                z(i+nx*ny*(m-1)) = zval(m);
                t(i+nx*ny*(m-1)) = timex;
            end 
        else
            for i = 1:nx
                x(i+nx*ny*(m-1)) = xval(nx+1-i);
                y(i+nx*ny*(m-1)) = yval(ny);
                z(i+nx*ny*(m-1)) = zval(m);
                t(i+nx*ny*(m-1)) = timex; 
            end  
        end
        for j = 1:ny-1
            x(j*nx+1+nx*ny*(m-1)) = x(j*nx+nx*ny*(m-1));
            y(j*nx+1+nx*ny*(m-1)) = yval(ny-j);
            z(j*nx+1+nx*ny*(m-1)) = zval(m);
            t(j*nx+1+nx*ny*(m-1)) = timey;
            for k = 2:nx
                if (mod(ny,2) == 0 && mod(j,2) == 0) || (mod(ny,2) ~= 0 && mod(j,2) ~= 0)
                    x(j*nx+k+nx*ny*(m-1)) = xval(k);
                else
                    x(j*nx+k+nx*ny*(m-1)) = xval(nx+1-k);
                end
                y(j*nx+k+nx*ny*(m-1)) = yval(ny-j);
                z(j*nx+k+nx*ny*(m-1)) = zval(m);
                t(j*nx+k+nx*ny*(m-1)) = timex;
            end
        end
    end
end    

c = {x y z t name tstep};
end